﻿using UnityEngine;
using System.Collections;

public class MoverControl : MonoBehaviour {

    public float speed;
	public Animator anim;
	Rigidbody2D Rigid2D;
	public Transform tr;
	public bool canMove;
	public bool isDead;
	bool hits;
	bool ishit;
	public GameObject OtherPlayer;  //initializing of the variables and referring to the Components

	void Start () {

		anim = GetComponent<Animator>();
		Rigid2D = GetComponent<Rigidbody2D>();
		tr = GetComponent<Transform> ();
		isDead = false;
		hits = false;
		ishit = false;
	}
		
	void FixedUpdate () {   //fixedUpdate is called once per frame

		if (canMove)   //can't move when dead
		{
			//Debug.Log (isDead);

			if (hits) //freezing when hitting with the sword
			{
				//Rigid2D.constraints = RigidbodyConstraints2D.FreezePositionX;
			}
			else 
			{
				Rigid2D.constraints = RigidbodyConstraints2D.None;
				Rigid2D.constraints = RigidbodyConstraints2D.FreezeRotation;  

			}


			if (Input.GetAxis ("Horizontal") > 0.1f && !hits) //moving forward
			{
				Rigid2D.AddForce (Vector2.right * speed);
				Rigid2D.AddForce (Vector2.up * 4); 
				anim.SetInteger ("speed", 1);  //changing the speed variable in the animator to call the transition for the next animation (moving forward)
			}

			if (Input.GetAxis ("Horizontal") < -0.1f && !hits) //moving backwards
			{
				Rigid2D.AddForce (Vector2.left * speed);
				Rigid2D.AddForce (Vector2.up * 4); 
				anim.SetInteger ("speed", -1);
			}

			if (Input.GetAxis ("Horizontal") == 0) //to call the idle animation, while doing nothing
			{
				anim.SetInteger ("speed", 0);
			}


			if (Input.GetButtonDown ("Hit Up") && !hits) //hit1
			{
				anim.SetInteger ("hit up", 1);
				hits = true;
			}
			if (Input.GetButtonDown("Hit Down") && !hits) //hit2
			{
				anim.SetInteger ("hit up", -1);
				hits = true;
			}


			if (Input.GetAxis ("Hit Up") == 0 && Input.GetAxis ("Hit Down") == 0) 
			{
				anim.SetInteger ("hit up", 0);   //switching back the hit-variable in the animator

				if (anim.GetInteger ("hit up") == 0 && hits && !ishit) 
				{
					ishit = true;

					StartCoroutine ("Hit");  //starting a coroutine for a little freeing time 
				}
			}

			if (Input.GetButtonDown("Vertical") && Rigid2D.IsTouchingLayers())  //allowing jumps while on ground
			{
				Rigid2D.AddForce (Vector2.up * 1050);
				anim.SetBool ("jump", true);


				StartCoroutine ("Jump");
				//Debug.Log ("jump!");

			} 
		}

	}

	IEnumerator Jump()
	{
		yield return new WaitForSeconds (0.1f);
		anim.SetBool ("jump", false);
	}


	IEnumerator Hit()
	{
		yield return new WaitForSeconds (2); //2 seconds of freezing time
		hits = false;
		ishit = false;
	}


	void OnTriggerEnter2D(Collider2D coll)  //called when the colliders touch
	{
		if (!isDead && !OtherPlayer.GetComponent<MoverControl2>().isDead) //only if both are alive
		{
			//Debug.Log ("Enter");
			if (coll.tag != "Sword" && hits) //when touching the sword of the enemy
			{
                Rigid2D.AddForce(Vector2.left * 1000);
                OtherPlayer.GetComponent<MoverControl2> ().isDead = true;    
				OtherPlayer.GetComponent<MoverControl2> ().canMove = false;  //the other player can neither move nor attack
				OtherPlayer.GetComponent<MoverControl2>().anim.SetBool ("die", true);  //starting the death animation
				anim.SetBool ("win", true);     
				StartCoroutine ("Die");
			}
		}

	}

	IEnumerator Die()
	{
        //starting the win-animation

		/**tr.Translate (Vector2.left * 3);
		tr.Translate (Vector2.up);**/						//to back of the enemy
		canMove = false;
		yield return new WaitForEndOfFrame();

		yield return new WaitForSeconds (5);
 		
		Application.LoadLevel (0);


	}

}