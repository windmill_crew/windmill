﻿using UnityEngine;
using System.Collections;

public class MoverControl2 : MonoBehaviour {     // !!!---For documentation see Movercontrol rather than this script--!!! 

    public float speed;
	public Animator anim;
	Rigidbody2D Rigid2D;
	public Transform tr;
	public bool canMove;
	public bool isDead;
	bool hits;
	bool ishit;
	public GameObject OtherPlayer;

	void Start () {

		anim = GetComponent<Animator>();
		Rigid2D = GetComponent<Rigidbody2D>();
		tr = GetComponent<Transform> ();
		isDead = false;
		hits = false;
		ishit = false;
	}

	void FixedUpdate () {

		if (canMove) 
		{
			//Debug.Log (isDead);

			if (hits) 
			{
				//Rigid2D.constraints = RigidbodyConstraints2D.FreezePositionX;
			}
			else 
			{
				Rigid2D.constraints = RigidbodyConstraints2D.None;
				Rigid2D.constraints = RigidbodyConstraints2D.FreezeRotation;

			}


			if (Input.GetAxis ("Horizontal2") > 0.1f && !hits) 
			{
				Rigid2D.AddForce (Vector2.right * speed);
				Rigid2D.AddForce (Vector2.up * 4); //für Treppen
				anim.SetInteger ("speed", -1);
			}

			if (Input.GetAxis ("Horizontal2") < -0.1f && !hits) 
			{
				Rigid2D.AddForce (Vector2.left * speed);
				Rigid2D.AddForce (Vector2.up * 4); //für Treppen
				anim.SetInteger ("speed", 1);
			}

			if (Input.GetAxis ("Horizontal2") == 0) 
			{
				anim.SetInteger ("speed", 0);
			}


			if (Input.GetButtonDown ("HitUp2") && !hits) 
			{
				anim.SetInteger ("hit up", 1);
				hits = true;
				//Debug.Log ("!!!");
			}
			if (Input.GetButtonDown("HitDown2") && !hits) 
			{
				anim.SetInteger ("hit up", -1);
				hits = true;
				//Debug.Log ("!!!!");
			}


			if (Input.GetAxis ("HitUp2") == 0 && Input.GetAxis ("HitDown2") == 0) 
			{
				anim.SetInteger ("hit up", 0);

				if (anim.GetInteger ("hit up") == 0 && hits && !ishit) 
				{
					ishit = true;

					StartCoroutine ("Hit");
				}
			}

			if (Input.GetButtonDown("Vertical2") && Rigid2D.IsTouchingLayers()) 
			{
				Rigid2D.AddForce (Vector2.up * 1050);
				anim.SetBool ("jump", true);


				StartCoroutine ("Jump");
				//Debug.Log ("jump!");

			}

		}

	}

	IEnumerator Jump()
	{
		yield return new WaitForSeconds (0.1f);
		anim.SetBool ("jump", false);
	}


	IEnumerator Hit()
	{
		yield return new WaitForSeconds (2);
		hits = false;
		ishit = false;
	}


	void OnTriggerEnter2D(Collider2D coll)
	{
		if (!isDead && !OtherPlayer.GetComponent<MoverControl>().isDead) 
		{
			Debug.Log ("Enter");
			if (coll.tag != "Sword" && hits) 
			{
                Rigid2D.AddForce(Vector2.left * 1000);
                OtherPlayer.GetComponent<MoverControl> ().isDead = true;
				OtherPlayer.GetComponent<MoverControl> ().canMove = false;
				OtherPlayer.GetComponent<MoverControl> ().anim.SetBool ("die", true);
				anim.SetBool ("win", true);
				StartCoroutine ("Die");
			}
		}

	}

	IEnumerator Die()
	{
        //Rigid2D.AddForce(Vector2.left * 1000);
		/**tr.Translate (Vector2.left * 3);
		tr.Translate (Vector2.left);**/
		canMove = false;
		yield return new WaitForEndOfFrame ();

		yield return new WaitForSeconds (5);

		Application.LoadLevel (0);
	}

}