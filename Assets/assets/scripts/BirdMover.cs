﻿using UnityEngine;
using System.Collections;

public class BirdMover : MonoBehaviour
{
    Rigidbody2D Rigid2D; //reference to the Rigidbody
	Transform tr; //reference to the Transform

    
    void Start ()  //called at the initialization of the script
    {
		Rigid2D = GetComponent<Rigidbody2D>();
		tr = GetComponent<Transform>();
    }
	

	void FixedUpdate ()  //fixedUpdate is called once per frame
    {		
        StartCoroutine("Fly");
		if (tr.position.x < -40)      //after moving away to far the bird clones itself and gets destroyed
		{
			Instantiate(tr, new Vector3(19.5f, 3.0f, 0), Quaternion.identity);
			Destroy (gameObject);
		}

		if (Input.GetButtonDown("endgame")) 
		{
			Application.Quit();
		}
	}


    IEnumerator Fly()
    {
		yield return new WaitForSeconds(10);
		Rigid2D.AddForce(Vector2.left * 18);
		Rigid2D.AddForce(Vector2.up * 1.5f);
		yield return new WaitForSeconds (20);   //adding the 'flight-force' to the bird
	}
		
		
}
