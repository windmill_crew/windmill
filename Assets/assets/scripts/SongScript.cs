﻿using UnityEngine;
using System.Collections;

public class SongScript : MonoBehaviour {

    public static bool cameras;
    Transform tr;
    AudioSource song;
	public AudioClip p1;
	public AudioClip p2;
	public AudioClip p3;   //references to the songs and the audio source


	void Start ()  //called at the initialization of the script
	{
        tr = GetComponent<Transform>();
        if (cameras)
        {
            Destroy(gameObject);
        }
        else
        {
            cameras = true;
            DontDestroyOnLoad(tr.gameObject);
        }               
		song = GetComponent<AudioSource>();
		StartCoroutine("LoopClips");
    }
	
	IEnumerator LoopClips()
	{
		while (true) 
		{

			int p = Random.Range (1, 3); //randomizing which song is to be played

			if (p == 1) 
			{
				song.clip = p1;
			}
			if (p == 2) 
			{
				song.clip = p2;
			}
			if (p == 3) 
			{
				song.clip = p3;
			}

			song.Play();

			yield return new WaitForSeconds (song.clip.length);  //waiting for the clip to end
		}
	}
}
